fastlane documentation
----

# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```sh
xcode-select --install
```

For _fastlane_ installation instructions, see [Installing _fastlane_](https://docs.fastlane.tools/#installing-fastlane)

# Available Actions

## iOS

### ios metrics

```sh
[bundle exec] fastlane ios metrics
```

Run all lanes

### ios build

```sh
[bundle exec] fastlane ios build
```

Build the application

### ios coverage

```sh
[bundle exec] fastlane ios coverage
```

Calculate the code coverage

### ios lint

```sh
[bundle exec] fastlane ios lint
```

Apply swift linting

### ios generate_coverage_report

```sh
[bundle exec] fastlane ios generate_coverage_report
```



----

This README.md is auto-generated and will be re-generated every time [_fastlane_](https://fastlane.tools) is run.

More information about _fastlane_ can be found on [fastlane.tools](https://fastlane.tools).

The documentation of _fastlane_ can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
