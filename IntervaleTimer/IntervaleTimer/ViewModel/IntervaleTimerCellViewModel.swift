//
//  IntervaleTimerCellViewModel.swift
//  IntervaleTimer
//
//  Created by Hadi KUDSI on 10/05/2023.
//

import Foundation

@MainActor class IntervaleTimerCellViewModel: ObservableObject, Identifiable, Equatable {

    let id: UUID
    @Published var workoutTimer: RepTimer
    @Published var pauseTimer: RepTimer
    @Published var restTimer: RepTimer
    @Published var workoutStatus: WorkoutStatus
    @Published var progressModel: CircularProgressModel
    @Published var timePickerModel: PickerModel
    @Published var pickerEnum: PickerEnum
    @Published var repetitionSet: RepetitionSet
    @Published var repetitionPickerModel: PickerModel
    @Published var title: String

    init() {
        self.id = UUID()
        self.workoutTimer = RepTimer()
        self.pauseTimer = RepTimer()
        self.restTimer = RepTimer()
        self.workoutStatus = .ended
        self.progressModel = CircularProgressModel()
        self.timePickerModel = PickerModel()
        self.pickerEnum = .workout
        self.repetitionSet = RepetitionSet()
        self.repetitionPickerModel = PickerModel()
        self.title = "Timer"
    }

    init(id: UUID,
         workoutTimer: RepTimer,
         pauseTimer: RepTimer,
         restTimer: RepTimer,
         workoutStatus: WorkoutStatus,
         progressModel: CircularProgressModel,
         timePickerModel: PickerModel,
         pickerEnum: PickerEnum,
         repetitionSet: RepetitionSet,
         repetitionPickerModel: PickerModel,
         title: String) {
        self.id = id
        self.workoutTimer = workoutTimer
        self.pauseTimer = pauseTimer
        self.restTimer = restTimer
        self.workoutStatus = workoutStatus
        self.progressModel = progressModel
        self.timePickerModel = timePickerModel
        self.pickerEnum = pickerEnum
        self.repetitionSet = repetitionSet
        self.repetitionPickerModel = repetitionPickerModel
        self.title = title
    }

    nonisolated static func == (lhs: IntervaleTimerCellViewModel, rhs: IntervaleTimerCellViewModel) -> Bool {
        return lhs.id == rhs.id
    }

    func save() {
        UserDefaultsManager.setIntervaleTimer(timer: self)
    }

    func setProgress() {
        self.progressModel.green = (self.workoutTimer.timeRemaining /
                                    self.workoutTimer.initialTime) * 0.5

        self.progressModel.red = (self.pauseTimer.timeRemaining /
                                  self.pauseTimer.initialTime) * 0.5

        self.progressModel.yellow = (self.restTimer.timeRemaining /
                                     self.restTimer.initialTime)
    }

    func timer() {
        self.setProgress()

        if self.repetitionSet.remainingRep > 0 {
            if self.repetitionSet.remainingSet > 0 {
                if self.workoutStatus == .start {
                    if self.restTimer.timeRemaining > 0 {
                        self.restTimer.timeRemaining -= Constant.timerUpdate
                    } else if self.workoutTimer.timeRemaining > 0 {
                        self.workoutTimer.timeRemaining -= Constant.timerUpdate
                    } else if self.pauseTimer.timeRemaining > 0 {
                        self.pauseTimer.timeRemaining -= Constant.timerUpdate
                    } else {
                        self.workoutTimer.timeRemaining = self.workoutTimer.initialTime
                        self.pauseTimer.timeRemaining = self.pauseTimer.initialTime
                        self.repetitionSet.remainingSet -= 1
                    }
                } else if self.workoutStatus == .pause {

                } else if self.workoutStatus == .stop {
                    self.workoutTimer.timeRemaining = self.workoutTimer.initialTime
                    self.pauseTimer.timeRemaining = self.pauseTimer.initialTime
                    self.restTimer.timeRemaining = self.restTimer.initialTime
                    self.repetitionSet.remainingSet = self.repetitionSet.initialSet
                    self.repetitionSet.remainingRep = self.repetitionSet.initialRep
                    self.workoutStatus = .ended
                }
            } else {
                if self.workoutStatus == .start {
                    if self.restTimer.timeRemaining > 0 {
                        self.restTimer.timeRemaining -= Constant.timerUpdate
                    } else {
                        self.restTimer.timeRemaining = self.restTimer.initialTime
                        self.repetitionSet.remainingSet = self.repetitionSet.initialSet
                        self.repetitionSet.remainingRep -= 1
                    }
                }
            }
        } else {
            self.workoutStatus = .ended
            self.repetitionSet.remainingRep = self.repetitionSet.initialRep
            self.repetitionSet.remainingSet = self.repetitionSet.initialSet
        }
    }

    func didDismissTimePicker() {
        if timePickerModel.doneTapped {
            timePickerDoneTapped()
        } else {
            timePickerDoneNotTapped()
        }
    }

    func timePickerDoneTapped() {
        switch pickerEnum {
        case .workout:
            workoutTimer.initialTime = CGFloat((workoutTimer.minutes * 60) +
                                               workoutTimer.seconds)
            workoutTimer.timeRemaining = workoutTimer.initialTime
            workoutTimer.oldSec = workoutTimer.seconds
            workoutTimer.minutes = workoutTimer.minutes

        case .pause:
            pauseTimer.initialTime = CGFloat((pauseTimer.minutes * 60) +
                                             pauseTimer.seconds)
            pauseTimer.timeRemaining = pauseTimer.initialTime
            pauseTimer.oldSec = pauseTimer.seconds
            pauseTimer.minutes = pauseTimer.minutes
        case .rest:
            restTimer.initialTime = CGFloat((restTimer.minutes * 60) +
                                            restTimer.seconds)
            restTimer.timeRemaining = restTimer.initialTime
            restTimer.oldSec = restTimer.seconds
            restTimer.minutes = restTimer.minutes

        }
        timePickerModel.doneTapped = false
        save()
    }

    func timePickerDoneNotTapped() {
        switch pickerEnum {
        case .workout:
            workoutTimer.seconds = workoutTimer.oldSec
            workoutTimer.minutes = workoutTimer.oldMin

        case .pause:
            pauseTimer.seconds = pauseTimer.oldSec
            pauseTimer.minutes = pauseTimer.oldMin

        case .rest:
            restTimer.seconds = restTimer.oldSec
            restTimer.minutes = restTimer.oldMin
        }
    }

    func didDismissRepetitionPicker() {
        if repetitionPickerModel.doneTapped {
            repetitionPickerDoneTapped()
        } else {
            repetitionPickerDoneNotTapped()
        }
    }

    func repetitionPickerDoneTapped() {
        repetitionSet.initialRep = repetitionSet.repetitions
        repetitionSet.remainingRep = repetitionSet.repetitions
        repetitionSet.oldRepetition = repetitionSet.repetitions

        repetitionSet.initialSet = repetitionSet.sets
        repetitionSet.remainingSet = repetitionSet.sets
        repetitionSet.oldSet = repetitionSet.sets

        repetitionPickerModel.doneTapped = false
        save()
    }

    func repetitionPickerDoneNotTapped() {
        repetitionSet.repetitions = repetitionSet.oldRepetition
        repetitionSet.sets = repetitionSet.oldSet
    }
}
