//
//  IntervaleTimerViewModel.swift
//  IntervaleTimer
//
//  Created by Hadi KUDSI on 19/04/2023.
//

import SwiftUI
import Combine

@MainActor class IntervaleTimerViewModel: ObservableObject {

    @Published var intervaleTimers: [IntervaleTimerCellViewModel]
    @Published var activePageIndex: Int
    @Published var listTimersId: [UUID]
    @Published var isTimerOn: Bool
    private var cancellables: Set<AnyCancellable> = []

    init() {
        self.listTimersId = UserDefaultsManager.listTimersId ?? []
        self.activePageIndex = 0
        self.intervaleTimers = [IntervaleTimerCellViewModel()]
        self.isTimerOn = false
        self.intervaleTimers.first?.objectWillChange.sink { [weak self] _ in
            self?.handleWorkoutStatusChange()
        }.store(in: &cancellables)
    }

    func addNewTimer() {
        let newTimer = IntervaleTimerCellViewModel()
        self.intervaleTimers.append(newTimer)
        self.listTimersId.append(newTimer.id)
        UserDefaultsManager.listTimersId = self.listTimersId
        UserDefaultsManager.setIntervaleTimer(timer: newTimer)
    }

    func loadTimers() {
        if !self.listTimersId.isEmpty {
            self.intervaleTimers = self.intervaleTimers.filter { $0 != self.intervaleTimers.first }
            for id in self.listTimersId {
                if let newTimer = UserDefaultsManager.getIntervaleTimer(forKey: id) {
                    self.intervaleTimers.append(newTimer)
                }
            }
        } else {
            if let timer = self.intervaleTimers.first {
                self.listTimersId.append(timer.id)
                UserDefaultsManager.listTimersId = self.listTimersId
                UserDefaultsManager.setIntervaleTimer(timer: timer)
            }
        }
        intervaleTimers.forEach { timer in
            timer.objectWillChange.sink { [weak self] _ in
                self?.handleWorkoutStatusChange()
            }.store(in: &cancellables)
        }
    }

    func deleteTimer() {
        self.listTimersId = self.listTimersId.filter { $0 != self.intervaleTimers[activePageIndex].id }
        UserDefaultsManager.listTimersId = self.listTimersId
        UserDefaultsManager.deleteIntervaleTimer(forKey: self.intervaleTimers[activePageIndex].id.uuidString)
        self.intervaleTimers = self.intervaleTimers.filter { $0 != self.intervaleTimers[activePageIndex] }
    }

    func handleWorkoutStatusChange() {
        var check = 0
        for timer in intervaleTimers {
            if timer.workoutStatus == .start || timer.workoutStatus == .pause {
                check += 1
            }
        }
        if check > 0 {
            self.isTimerOn = true
        } else {
            self.isTimerOn = false
        }
    }
}
