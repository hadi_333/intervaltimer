//
//  ContentView.swift
//  IntervaleTimer
//
//  Created by Hadi KUDSI on 18/04/2023.
//

import SwiftUI
import UIKit

struct ContentView: View {

    @StateObject private var viewModel = IntervaleTimerViewModel()
    @State private var keyboardHeight: CGFloat = 0

    var body: some View {
        NavigationStack {
            ZStack {
                DottedBackgroundView()
                VStack {
                    HStack {
                        if viewModel.activePageIndex <= viewModel.intervaleTimers.count - 1 {
                            SetsHolder(viewModel: viewModel.intervaleTimers[viewModel.activePageIndex])
                                .frame(width: 400,
                                       height: 1)
                        } else {
                            SetsHolder(viewModel: IntervaleTimerCellViewModel())
                                .frame(width: 400,
                                       height: 1)

                        }
                    }
                    VStack {
                        GeometryReader { geometry in
                            AdaptivePagingScrollView(currentPageIndex: $viewModel.activePageIndex,
                                                     itemsAmount: $viewModel.intervaleTimers.count,
                                                     itemWidth: 350,
                                                     itemPadding: 0,
                                                     pageWidth: geometry.size.width,
                                                     disableGesture: $viewModel.isTimerOn) {
                                ForEach($viewModel.intervaleTimers) { $cell in
                                    GeometryReader { screen in
                                        IntervaleTimerCell(viewModel: cell)
                                            .rotation3DEffect(Angle(degrees:
                                                                        (Double(screen.frame(in: .global).minX) - 20) / -15),
                                                              axis: (x: 0, y: 90, z: 90))
                                            .scaleEffect(viewModel.activePageIndex ==
                                                         viewModel.intervaleTimers.firstIndex(of: cell) ?
                                                         1.05 : 1)
                                    }
                                }
                                ZStack {
                                    Circle()
                                        .fill(Color.green)
                                        .frame(width: 200, height: 200)
                                    Text("+")
                                        .font(.system(size: 50, weight: .bold))
                                        .foregroundColor(.white)
                                }
                                .onTapGesture {
                                    withAnimation(.easeInOut(duration: 0.5)) {
                                        viewModel.addNewTimer()
                                    }
                                }
                                .onAppear {
                                    viewModel.loadTimers()
                                    UIApplication.shared.isIdleTimerDisabled = true
                                }
                            }
                        }
                        .frame(height: 400)

                        HStack {
                            PageControl(list: $viewModel.intervaleTimers,
                                        currentPage: $viewModel.activePageIndex)
                            .aspectRatio(3 / 2, contentMode: .fit)
                            .frame(width: 165, height: 42)
                            .background(RadialGradient(gradient: Gradient(colors: [.gray, .clear]),
                                                       center: .center,
                                                       startRadius: 15,
                                                       endRadius: 100))
                            .disabled(true)
                        }

                        Spacer()
                        if viewModel.activePageIndex > viewModel.intervaleTimers.count - 1 {
                            withAnimation(.easeOut(duration: 0.3)) {
                                WorkoutButtonView(cell: IntervaleTimerCellViewModel())
                                    .padding(.bottom)
                                    .isHidden(true)
                            }
                        } else {
                            WorkoutButtonView(cell: viewModel.intervaleTimers[viewModel.activePageIndex])
                                .padding(.bottom)
                        }
                    }
                    .toolbar {
                        if viewModel.activePageIndex <= viewModel.intervaleTimers.count - 1 {
                            ToolbarItem(placement: .navigationBarLeading) {
                                ZStack {
                                    Circle()
                                        .frame(width: 30, height: 30)
                                        .foregroundColor(.red)
                                    Text("-")
                                        .font(.system(size: 30, weight: .bold))
                                        .foregroundColor(.white)
                                }
                                .padding(.leading)
                                .simultaneousGesture(
                                    LongPressGesture(minimumDuration: 1.0).onEnded({ _ in
                                        withAnimation {
                                            viewModel.deleteTimer()
//                                          UserDefaultsManager.deleteAllStoredData()
                                        }
                                    })
                                )
                            }
                            ToolbarItem(placement: .principal) {
                                TextField(viewModel.intervaleTimers[viewModel.activePageIndex].title,
                                          text: $viewModel.intervaleTimers[viewModel.activePageIndex].title)
                                .bold()
                                .foregroundColor(.white)
                                .multilineTextAlignment(.center)
                                .padding(.bottom)
                            }
                            ToolbarItem(placement: .navigationBarTrailing) {
                                Circle()
                                    .frame(width: 30, height: 30)
                                    .padding(.trailing)
                                    .isHidden(true)
                            }
                        } else {
                            ToolbarItem(placement: .principal) {
                                Text("Title")
                                .bold()
                                .foregroundColor(.white)
                                .multilineTextAlignment(.center)
                                .padding(.bottom)
                            }
                        }
                    }
                }
                .padding(.bottom, keyboardHeight)
                .onReceive(NotificationCenter.default
                    .publisher(for: UIResponder.keyboardWillShowNotification)) { notification in
                        guard let keyboardFrame = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as?
                                CGRect else { return }
                        keyboardHeight = -1000
                    }
                    .onReceive(NotificationCenter.default.publisher(for: UIResponder.keyboardWillHideNotification)) { _ in
                        keyboardHeight = 0
                    }
            }
            .background(.black)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
