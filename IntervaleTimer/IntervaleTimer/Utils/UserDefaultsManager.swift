//
//  UserDefaultsManager.swift
//  IntervaleTimer
//
//  Created by Hadi KUDSI on 15/05/2023.
//

import Foundation

struct UserDefaultsManager {
    static let defaults = UserDefaults.standard

    static func deleteAllStoredData() {
        if let bundleId = Bundle.main.bundleIdentifier {
            UserDefaults.standard.removePersistentDomain(forName: bundleId)
        }
    }

    static var listTimersId: [UUID]? {
        get {
            if let uuidStrings = defaults.object(forKey: DefaultsKeys.listTimersId) as? [String] {
                return uuidStrings.map { UUID(uuidString: $0) ?? UUID() }
            } else {
                return nil
            }
        }
        set {
            let uuidStrings = newValue?.map { $0.uuidString }
            defaults.set(uuidStrings, forKey: DefaultsKeys.listTimersId)
        }
    }

    @MainActor static func getIntervaleTimer(forKey key: UUID) -> IntervaleTimerCellViewModel? {
        if let workoutTimer = getRepTimer(forKey: DefaultsKeys.workoutTimer(key.uuidString)),
           let pauseTimer = getRepTimer(forKey: DefaultsKeys.pauseTimer(key.uuidString)),
           let restTimer = getRepTimer(forKey: DefaultsKeys.restTimer(key.uuidString)),
           let workoutStatus = getWorkoutStatus(forKey: DefaultsKeys.workoutStatus(key.uuidString)),
           let progressModel = getProgressModel(forKey: DefaultsKeys.progressModel(key.uuidString)),
           let timePickerModel = getPickerModel(forKey: DefaultsKeys.timePickerModel(key.uuidString)),
           let pickerEnum = getPickerEnum(forKey: DefaultsKeys.pickerEnum(key.uuidString)),
           let repetitionSet = getRepetitionSet(forKey: DefaultsKeys.repetitionSet(key.uuidString)),
           let repetitionPickerModel = getPickerModel(forKey: DefaultsKeys.repetitionPickerModel(key.uuidString)),
           let title = getTitle(forKey: DefaultsKeys.title(key.uuidString)) {

            let newTimer = IntervaleTimerCellViewModel(id: key,
                                                       workoutTimer: workoutTimer,
                                                       pauseTimer: pauseTimer,
                                                       restTimer: restTimer,
                                                       workoutStatus: workoutStatus,
                                                       progressModel: progressModel,
                                                       timePickerModel: timePickerModel,
                                                       pickerEnum: pickerEnum,
                                                       repetitionSet: repetitionSet,
                                                       repetitionPickerModel: repetitionPickerModel,
                                                       title: title)

            return newTimer
        }
        return nil
    }

    @MainActor static func setIntervaleTimer(timer: IntervaleTimerCellViewModel) {
        let newID = timer.id.uuidString
        self.setRepTimer(timer.workoutTimer, forKey: DefaultsKeys.workoutTimer(newID))
        self.setRepTimer(timer.pauseTimer, forKey: DefaultsKeys.pauseTimer(newID))
        self.setRepTimer(timer.restTimer, forKey: DefaultsKeys.restTimer(newID))
        self.setWorkoutStatus(timer.workoutStatus, forKey: DefaultsKeys.workoutStatus(newID))
        self.setProgressModel(timer.progressModel, forKey: DefaultsKeys.progressModel(newID))
        self.setPickerModel(timer.timePickerModel, forKey: DefaultsKeys.timePickerModel(newID))
        self.setPickerEnum(timer.pickerEnum, forKey: DefaultsKeys.pickerEnum(newID))
        self.setRepetitionSet(timer.repetitionSet, forKey: DefaultsKeys.repetitionSet(newID))
        self.setPickerModel(timer.repetitionPickerModel,
                            forKey: DefaultsKeys.repetitionPickerModel(newID))
        self.setTitle(timer.title, forKey: DefaultsKeys.title(newID))
    }

    static func deleteIntervaleTimer(forKey key: String) {
        defaults.removeObject(forKey: DefaultsKeys.workoutTimer(key))
        defaults.removeObject(forKey: DefaultsKeys.pauseTimer(key))
        defaults.removeObject(forKey: DefaultsKeys.restTimer(key))
        defaults.removeObject(forKey: DefaultsKeys.workoutStatus(key))
        defaults.removeObject(forKey: DefaultsKeys.progressModel(key))
        defaults.removeObject(forKey: DefaultsKeys.timePickerModel(key))
        defaults.removeObject(forKey: DefaultsKeys.pickerEnum(key))
        defaults.removeObject(forKey: DefaultsKeys.repetitionSet(key))
        defaults.removeObject(forKey: DefaultsKeys.repetitionPickerModel(key))
        defaults.removeObject(forKey: DefaultsKeys.title(key))
    }

    static func setRepTimer(_ repTimer: RepTimer, forKey: String) {
        try? defaults.setObject(repTimer, forKey: forKey)
    }

    static func getRepTimer(forKey: String) -> RepTimer? {
        return try? defaults.getObject(forKey: forKey, castTo: RepTimer.self)
    }
    
    static func setWorkoutStatus(_ workoutStatus: WorkoutStatus, forKey: String) {
        try? defaults.setObject(workoutStatus, forKey: forKey)
    }

    static func getWorkoutStatus(forKey: String) -> WorkoutStatus? {
        return try? defaults.getObject(forKey: forKey, castTo: WorkoutStatus.self)
    }

    static func setProgressModel(_ progressModel: CircularProgressModel, forKey: String) {
        try? defaults.setObject(progressModel, forKey: forKey)
    }

    static func getProgressModel(forKey: String) -> CircularProgressModel? {
        return try? defaults.getObject(forKey: forKey, castTo: CircularProgressModel.self)
    }

    static func setPickerModel(_ pickerModel: PickerModel, forKey: String) {
        try? defaults.setObject(pickerModel, forKey: forKey)
    }

    static func getPickerModel(forKey: String) -> PickerModel? {
        return try? defaults.getObject(forKey: forKey, castTo: PickerModel.self)
    }

    static func setPickerEnum(_ pickerEnum: PickerEnum, forKey: String) {
        try? defaults.setObject(pickerEnum, forKey: forKey)
    }

    static func getPickerEnum(forKey: String) -> PickerEnum? {
        return try? defaults.getObject(forKey: forKey, castTo: PickerEnum.self)
    }

    static func setRepetitionSet(_ repetitionSet: RepetitionSet, forKey: String) {
        try? defaults.setObject(repetitionSet, forKey: forKey)
    }

    static func getRepetitionSet(forKey: String) -> RepetitionSet? {
        return try? defaults.getObject(forKey: forKey, castTo: RepetitionSet.self)
    }
    static func setTitle(_ title: String, forKey: String) {
        try? defaults.setObject(title, forKey: forKey)
    }

    static func getTitle(forKey: String) -> String? {
        return try? defaults.getObject(forKey: forKey, castTo: String.self)
    }
}
