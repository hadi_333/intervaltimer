//
//  DefaultsKeys.swift
//  IntervaleTimer
//
//  Created by Hadi KUDSI on 15/05/2023.
//

import Foundation

struct DefaultsKeys {
    static let intervaleTimers = "intervaleTimers"
    static let listTimersId = "listTimersId"
    static let timer = "intervaleTimer"

    static func workoutTimer(_ key: String) -> String {
        return "workoutTimer: " + key
    }
    static func pauseTimer(_ key: String) -> String {
        return "pauseTimer: " + key
    }
    static func restTimer(_ key: String) -> String {
        return "restTimer: " + key
    }
    static func workoutStatus(_ key: String) -> String {
        return "workoutStatus: " + key
    }
    static func progressModel(_ key: String) -> String {
        return "progressModel: " + key
    }
    static func timePickerModel(_ key: String) -> String {
        return "timePickerModel: " + key
    }
    static func pickerEnum(_ key: String) -> String {
        return "pickerEnum: " + key
    }
    static func repetitionSet(_ key: String) -> String {
        return "repetitionSet: " + key
    }
    static func repetitionPickerModel(_ key: String) -> String {
        return "repetitionPickerModel: " + key
    }
    static func title(_ key: String) -> String {
        return "title: " + key
    }
}
