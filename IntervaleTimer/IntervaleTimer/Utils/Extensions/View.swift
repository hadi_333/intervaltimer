//
//  View.swift
//  IntervaleTimer
//
//  Created by Hadi KUDSI on 19/04/2023.
//

import SwiftUI

extension View {
    /// Hide or show the view based on a boolean value.
    ///
    /// - Parameters:
    ///   - hidden: Set to `false` to show the view. Set to `true` to hide the view.
    ///   - remove: Boolean value indicating whether or not to remove the view.
    @ViewBuilder func isHidden(_ hidden: Bool, remove: Bool = false) -> some View {
        if hidden {
            if !remove {
                self.hidden()
            }
        } else {
            self
        }
    }
}
