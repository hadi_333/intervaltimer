//
//  IntervaleTimerApp.swift
//  IntervaleTimer
//
//  Created by Hadi KUDSI on 18/04/2023.
//

import SwiftUI

@main
struct IntervaleTimerApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
