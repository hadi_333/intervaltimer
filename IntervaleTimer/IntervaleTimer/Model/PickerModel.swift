//
//  TimePickerModel.swift
//  IntervaleTimer
//
//  Created by Hadi KUDSI on 20/04/2023.
//

import Foundation

struct PickerModel: Codable {
    var isPickerShown: Bool
    var doneTapped: Bool

    init(isPickerShown: Bool = false,
         doneTapped: Bool = false) {
        self.isPickerShown = isPickerShown
        self.doneTapped = doneTapped
    }
}
