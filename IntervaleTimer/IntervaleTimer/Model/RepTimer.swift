//
//  WorkoutTimer.swift
//  IntervaleTimer
//
//  Created by Hadi KUDSI on 20/04/2023.
//

import Foundation

struct RepTimer: Codable {
    var initialTime: CGFloat
    var timeRemaining: CGFloat
    var minutes: Int
    var seconds: Int
    var oldMin: Int
    var oldSec: Int

    init(initialTime: CGFloat = 1,
         timeRemaning: CGFloat = 1,
         minutes: Int = 0,
         seconds: Int = 1,
         oldMin: Int = 0,
         oldSec: Int = 1) {
        self.initialTime = initialTime
        self.timeRemaining = timeRemaning
        self.minutes = minutes
        self.seconds = seconds
        self.oldMin = oldMin
        self.oldSec = oldSec
    }
}
