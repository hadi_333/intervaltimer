//
//  PickerEnum.swift
//  IntervaleTimer
//
//  Created by Hadi KUDSI on 28/04/2023.
//

import Foundation

enum PickerEnum: Codable {
    case workout
    case pause
    case rest
}
