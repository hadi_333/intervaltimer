//
//  WorkoutStatus.swift
//  IntervaleTimer
//
//  Created by Hadi KUDSI on 20/04/2023.
//

import Foundation

enum WorkoutStatus: Codable {
    case start
    case pause
    case stop
    case ended
}
