//
//  CircularProgressModel.swift
//  IntervaleTimer
//
//  Created by Hadi KUDSI on 20/04/2023.
//

import Foundation

struct CircularProgressModel: Codable {
    var green: CGFloat
    var red: CGFloat
    var yellow: CGFloat
    
    init(green: CGFloat = 0,
         red: CGFloat = 0,
         yellow: CGFloat = 0) {
        self.green = green
        self.red = red
        self.yellow = yellow
    }
}
