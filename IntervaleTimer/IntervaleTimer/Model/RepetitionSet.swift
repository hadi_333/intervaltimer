//
//  RepetitionSet.swift
//  IntervaleTimer
//
//  Created by Hadi KUDSI on 02/05/2023.
//

import Foundation

struct RepetitionSet: Codable {
    var initialRep: Int
    var remainingRep: Int
    var initialSet: Int
    var remainingSet: Int
    var repetitions: Int
    var oldRepetition: Int
    var sets: Int
    var oldSet: Int

    init(initialRep: Int = 1,
         remainingRep: Int = 1,
         initialSet: Int = 1,
         remainingSet: Int = 1,
         newRepetition: Int = 1,
         oldRepetition: Int = 1,
         newSet: Int = 1,
         oldSet: Int = 1) {
        self.initialRep = initialRep
        self.remainingRep = remainingRep
        self.initialSet = initialSet
        self.remainingSet = remainingSet
        self.repetitions = newRepetition
        self.oldRepetition = oldRepetition
        self.sets = newSet
        self.oldSet = oldSet
    }
}
