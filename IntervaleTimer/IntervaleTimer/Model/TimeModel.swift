//
//  TimeModel.swift
//  IntervaleTimer
//
//  Created by Hadi KUDSI on 20/04/2023.
//

import Foundation

struct TimeModel {
    var minutes: Int
    var seconds: Int

    init(minutes: Int,
         seconds: Int) {
        self.minutes = minutes
        self.seconds = seconds
    }
}
