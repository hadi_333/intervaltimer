//
//  WorkoutButtonView.swift
//  IntervaleTimer
//
//  Created by Hadi KUDSI on 19/04/2023.
//

import SwiftUI

struct WorkoutButtonView: View {

    @ObservedObject var cell: IntervaleTimerCellViewModel

    var body: some View {
        Button(action: {
            cell.workoutStatus = .start
        }) {
            Spacer()
                .frame(width: 30)
            HStack {
                Spacer()
                Text(R.string.localizable.start_workout())
                    .foregroundColor(.black)
                    .font(.headline)
                Spacer()
            }
            .padding()
            .frame(height: 50)
            .background(Color.green)
            .cornerRadius(10)
            .contentShape(Rectangle())
            Spacer()
                .frame(width: 30)
        }
        .isHidden(cell.workoutStatus == .start ||
                  cell.workoutStatus == .pause,
                  remove: cell.workoutStatus == .start ||
                  cell.workoutStatus == .pause)

        HStack {
            Button(action: {
                if cell.workoutStatus == .start {
                    cell.workoutStatus = .pause
                } else if cell.workoutStatus == .pause {
                    cell.workoutStatus = .start
                }
            }) {
                Spacer()
                    .frame(width: 30)
                HStack {
                    if cell.workoutStatus == .pause {
                        Spacer()
                        Text(R.string.localizable.resume_workout())
                            .foregroundColor(.black)
                            .font(.subheadline)
                            .lineLimit(1)
                            .minimumScaleFactor(0.01)
                        Spacer()
                    } else {
                        Spacer()
                        Text(R.string.localizable.pause_workout())
                            .foregroundColor(.black)
                            .font(.subheadline)
                            .lineLimit(1)
                            .minimumScaleFactor(0.01)
                        Spacer()
                    }
                }
                .padding()
                .frame(height: 50)
                .background(Color.gray)
                .cornerRadius(10)
                .contentShape(Rectangle())

            }

            Button(action: {
                cell.workoutStatus = .stop
            }) {
                HStack {
                    Spacer()
                    Text(R.string.localizable.stop_workout())
                        .foregroundColor(.black)
                        .font(.subheadline)
                        .lineLimit(1)
                        .minimumScaleFactor(0.01)

                    Spacer()
                }
                .padding()
                .frame(height: 50)
                .background(Color.red)
                .cornerRadius(10)
                .contentShape(Rectangle())
                Spacer()
                    .frame(width: 30)
            }
            .frame(width: 200)
        }
        .isHidden(cell.workoutStatus != .start &&
                  cell.workoutStatus != .pause,
                  remove: cell.workoutStatus != .start &&
                  cell.workoutStatus != .pause)
    }
}

struct WorkoutButtonView_Previews: PreviewProvider {
    static var previews: some View {
        WorkoutButtonView(cell: IntervaleTimerCellViewModel())
    }
}
