//
//  CircleText.swift
//  IntervaleTimer
//
//  Created by Hadi KUDSI on 06/06/2023.
//
//  https://medium.com/dwarves-foundation/create-circular-text-using-swiftui-32cd7e5b6414

import SwiftUI

struct CircleText: View {
    var text: String
    var radius: Double
    var rotationDegree: Double
    var flip: Bool
    var kerning: CGFloat = 5.0

    private var texts: [(offset: Int, element: Character)] {
        if flip {
            return Array(text.uppercased().reversed().enumerated())
        } else {
            return Array(text.uppercased().enumerated())
        }
    }

    private var rotation3DDegree: Double {
        if flip {
            return 180
        } else {
            return 0
        }
    }

    @State var textSizes: [Int: Double] = [:]

    var body: some View {
        ZStack {
            ForEach(self.texts, id: \.self.offset) { (offset, element) in
                VStack {
                    Text(String(element))
                        .rotation3DEffect(.degrees(rotation3DDegree), axis: (x: 1, y: 0, z: 10))
                        .kerning(self.kerning)
                        .background(Sizeable())
                        .onPreferenceChange(WidthPreferenceKey.self,
                                            perform: { size in
                            self.textSizes[offset] = Double(size)
                        })
                    Spacer()
                }
                .rotationEffect(self.angle(at: offset))
            }
        }
//        .rotationEffect(-self.angle(at: self.text.count-1)/2)
        .rotationEffect(.degrees(rotationDegree))
        .frame(width: 250, height: 250, alignment: .center)
    }

    private func angle(at index: Int) -> Angle {
        guard let labelSize = textSizes[index] else {return .radians(0)}
        let percentOfLabelInCircle = labelSize / radius.perimeter
        let labelAngle = 2 * Double.pi * percentOfLabelInCircle

        let totalSizeOfPreChars = textSizes.filter { $0.key < index }.map { $0.value }.reduce(0, +)
        let percenOfPreCharInCircle = totalSizeOfPreChars / radius.perimeter
        let angleForPreChars = 2 * Double.pi * percenOfPreCharInCircle

        return .radians(angleForPreChars + labelAngle)
    }
}

extension Double {
    var perimeter: Double {
        return self * 2 * .pi
    }
}

struct WidthPreferenceKey: PreferenceKey {
    typealias Value = CGFloat
    static var defaultValue = CGFloat(0)
    static func reduce(value: inout CGFloat, nextValue: () -> CGFloat) {
        value = nextValue()
    }
}

struct Sizeable: View {
    var body: some View {
        GeometryReader { geometry in
            Color.clear
                .preference(key: WidthPreferenceKey.self,
                            value: geometry.size.width)
        }
    }
}

struct CircleText_Previews: PreviewProvider {
    static var previews: some View {
        CircleText(text: "t'ill i collapse", radius: 150, rotationDegree: 90, flip: false)
        CircleText(text: "t'ill i collapse", radius: 150, rotationDegree: 90, flip: true)
        CircleText(text: R.string.localizable.workout_title(),
                   radius: 100,
                   rotationDegree: 90,
                   flip: true)
    }
}
