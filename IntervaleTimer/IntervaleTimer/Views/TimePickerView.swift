//
//  TimePickerView.swift
//  IntervaleTimer
//
//  Created by Hadi KUDSI on 19/04/2023.
//

import SwiftUI

struct TimePickerView: View {

    @Binding var timePickerModel: PickerModel
    @Binding var repTimer: RepTimer

    var body: some View {
        NavigationView {
            HStack {
                Spacer()

                Picker("", selection: $repTimer.minutes) {
                    ForEach(0..<60, id: \.self) { input in
                        Text("\(input) " + R.string.localizable.min_title()).tag(input)
                    }
                }
                .pickerStyle(WheelPickerStyle())
                .frame(width: 180)
                .clipped()

                Picker("", selection: $repTimer.seconds) {
                    ForEach(0..<60, id: \.self) { input in
                        Text("\(input) " + R.string.localizable.sec_title()).tag(input)
                    }
                }
                .pickerStyle(WheelPickerStyle())
                .frame(width: 180)
                .clipped()
                .onChange(of: repTimer.seconds) { _ in
                    if repTimer.minutes == 0 &&
                        repTimer.seconds == 0 {
                        withAnimation {
                            repTimer.seconds = 1
                        }
                    }
                }
                Spacer()
            }
            .navigationTitle(Text(""))
            .navigationBarItems(trailing:
                                    Button(action: {
                timePickerModel.isPickerShown = false
                timePickerModel.doneTapped = true
            }) {
                Text(R.string.localizable.done_title())
                    .font(.headline)
                    .foregroundColor(.green)
            })
        }
    }
}

struct TimePickerView_Previews: PreviewProvider {
    static var previews: some View {
        TimePickerView(timePickerModel: .constant(PickerModel(isPickerShown: true, doneTapped: true)
                                                 ),
                       repTimer: .constant(RepTimer()))
    }
}
