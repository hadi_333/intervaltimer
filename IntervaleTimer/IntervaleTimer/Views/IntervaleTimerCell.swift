//
//  IntervaleTimerCell.swift
//  IntervaleTimer
//
//  Created by Hadi KUDSI on 10/05/2023.
//

import SwiftUI

struct IntervaleTimerCell: View {

    @ObservedObject var viewModel: IntervaleTimerCellViewModel
    let timer = Timer.publish(every: Constant.timerUpdate,
                              on: .main,
                              in: .common)
        .autoconnect()

    var body: some View {
        GeometryReader { geometry in
            VStack {
                ZStack {
                    if viewModel.workoutStatus != .ended {
                        CircularProgressView(progress: $viewModel.progressModel,
                                             workoutTime: $viewModel.workoutTimer.timeRemaining,
                                             pauseTime: $viewModel.pauseTimer.timeRemaining,
                                             restTime: $viewModel.restTimer.timeRemaining)
                        .frame(width: 250, height: 250)
                        .onReceive(timer) { _ in
                            viewModel.timer()
                        }
                    }
                    CircularButtonView(isPickerShown: $viewModel.timePickerModel.isPickerShown,
                                       pickerEnum: $viewModel.pickerEnum,
                                       workoutTime: $viewModel.workoutTimer.timeRemaining,
                                       pauseTime: $viewModel.pauseTimer.timeRemaining,
                                       restTime: $viewModel.restTimer.timeRemaining)
                    .frame(width: 250, height: 250)
                    .isHidden(viewModel.workoutStatus == .start ||
                              viewModel.workoutStatus == .pause)
                    .sheet(isPresented: $viewModel.timePickerModel.isPickerShown,
                           onDismiss: {
                        viewModel.didDismissTimePicker()
                    }) {
                        switch viewModel.pickerEnum {
                        case .workout:
                            TimePickerView(timePickerModel: $viewModel.timePickerModel,
                                           repTimer: $viewModel.workoutTimer)
                            .presentationDetents([.fraction(0.4)])

                        case.pause:
                            TimePickerView(timePickerModel: $viewModel.timePickerModel,
                                           repTimer: $viewModel.pauseTimer)
                            .presentationDetents([.fraction(0.4)])

                        case .rest:
                            TimePickerView(timePickerModel: $viewModel.timePickerModel,
                                           repTimer: $viewModel.restTimer)
                            .presentationDetents([.fraction(0.4)])
                        }
                    }
                }

                TimeView(repetitionSet: $viewModel.repetitionSet,
                         repPickerModel: $viewModel.repetitionPickerModel)
                .sheet(isPresented: $viewModel.repetitionPickerModel.isPickerShown,
                       onDismiss: {
                    viewModel.didDismissRepetitionPicker()
                }) {
                    RepetitionsPickerView(repetitionSet: $viewModel.repetitionSet,
                                          repetitionPicker: $viewModel.repetitionPickerModel)
                    .presentationDetents([.fraction(0.4)])
                }
            }
            .frame(width: geometry.size.width,
                   height: 400,
                   alignment: .center)
        }
    }
}

struct IntervaleTimerCell_Previews: PreviewProvider {
    static var previews: some View {
        IntervaleTimerCell(viewModel: (IntervaleTimerCellViewModel()))
    }
}
