//
//  CircularButtonView.swift
//  IntervaleTimer
//
//  Created by Hadi KUDSI on 19/04/2023.
//

import SwiftUI

struct CircularButtonView: View {

    @Binding var isPickerShown: Bool
    @Binding var pickerEnum: PickerEnum
    @Binding var workoutTime: CGFloat
    var workoutTimeString: String {
        String(format: "%02d:%02d", Int(workoutTime / 60),
               Int(workoutTime.truncatingRemainder(dividingBy: 60)))
    }
    @Binding var pauseTime: CGFloat
    var pauseTimeString: String {
        String(format: "%02d:%02d", Int(pauseTime / 60),
               Int(pauseTime.truncatingRemainder(dividingBy: 60)))
    }
    @Binding var restTime: CGFloat
    var restTimeString: String {
        String(format: "%02d:%02d", Int(restTime / 60),
               Int(restTime.truncatingRemainder(dividingBy: 60)))
    }
    @State private var isWorkoutTapped = false
    @State private var isPauseTapped = false
    @State private var isRestTapped = false

    var body: some View {
        ZStack {
            GeometryReader { geometry in
                let size = min(geometry.size.width, geometry.size.height)
                let lineWidth: CGFloat = 50

                Circle()
                    .stroke(Color.red,
                            lineWidth: lineWidth + 5)
                    .frame(width: size - lineWidth,
                           height: size - lineWidth)
                    .position(x: geometry.size.width / 2,
                              y: geometry.size.height / 2)

                Circle()
                    .stroke(Color.black,
                            lineWidth: lineWidth)
                    .opacity(0.8)
                    .frame(width: size - lineWidth, height: size - lineWidth)
                    .position(x: geometry.size.width / 2,
                              y: geometry.size.height / 2)

                Circle()
                    .trim(from: 0, to: 0.375)
                    .stroke(Color.green,
                            lineWidth: lineWidth)
                    .frame(width: size - lineWidth,
                           height: size - lineWidth)
                    .rotationEffect(.degrees(-45))
                    .position(x: geometry.size.width / 2,
                              y: geometry.size.height / 2)
                    .onTapGesture {
                        isPickerShown = true
                        pickerEnum = .workout
                    }
                    .scaleEffect(isWorkoutTapped ? 1.1 : 1.0)
                    .onChange(of: isPickerShown) { newValue in
                        if newValue {
                            withAnimation {
                                isWorkoutTapped = pickerEnum == .workout
                            }
                        } else {
                            withAnimation {
                                isWorkoutTapped = false
                            }
                        }
                    }

                Circle()
                    .trim(from: 0, to: 0.375)
                    .stroke(Color.red,
                            lineWidth: lineWidth)
                    .frame(width: size - lineWidth,
                           height: size - lineWidth)
                    .rotationEffect(.degrees(90))
                    .position(x: geometry.size.width / 2,
                              y: geometry.size.height / 2)
                    .onTapGesture {
                        isPickerShown = true
                        pickerEnum = .pause
                    }
                    .scaleEffect(isPauseTapped ? 1.1 : 1.0)
                    .onChange(of: isPickerShown) { newValue in
                        if newValue {
                            withAnimation {
                                isPauseTapped = pickerEnum == .pause
                            }
                        } else {
                            withAnimation {
                                isPauseTapped = false
                            }
                        }
                    }

                Circle()
                    .trim(from: 0, to: 0.25)
                    .stroke(Color.yellow,
                            lineWidth: lineWidth)
                    .frame(width: size - lineWidth,
                           height: size - lineWidth)
                    .rotationEffect(.degrees(225))
                    .position(x: geometry.size.width / 2,
                              y: geometry.size.height / 2)
                    .onTapGesture {
                        isPickerShown = true
                        pickerEnum = .rest
                    }
                    .scaleEffect(isRestTapped ? 1.1 : 1.0)
                    .onChange(of: isPickerShown) { newValue in
                        if newValue {
                            withAnimation {
                                isRestTapped = pickerEnum == .rest
                            }
                        } else {
                            withAnimation {
                                isRestTapped = false
                            }
                        }
                    }

                Circle()
                    .fill(Color.gray)
                    .frame(width: size - lineWidth,
                           height: size - lineWidth)
                    .position(x: geometry.size.width / 2,
                              y: geometry.size.height / 2)
                VStack {
                    HStack {
                        VStack {
                            Text(pauseTimeString)
                                .bold()
                                .foregroundColor(.white)
                            Text(R.string.localizable.pause_title())
                                .font(.caption)
                                .foregroundColor(.white)
                        }
                        Spacer()
                        VStack {
                            Text(restTimeString)
                                .bold()
                                .foregroundColor(.white)
                            Text(R.string.localizable.rest_title())
                                .font(.caption)
                                .foregroundColor(.white)
                        }
                    }
                    .frame(width: 150)
                    VStack {
                        Text(workoutTimeString)
                            .font(.system(size: 60, weight: .bold))
                            .foregroundColor(.white)
                        Text(R.string.localizable.workout_title())
                            .foregroundColor(.white)
                    }
                }
                .position(x: geometry.size.width / 2,
                          y: geometry.size.height / 2)

                CircleText(text: R.string.localizable.workout_title(),
                           radius: 150,
                           rotationDegree: 85,
                           flip: true)
                .position(x: geometry.size.width / 2,
                          y: geometry.size.height / 2)
                .frame(width: geometry.size.width,
                       height: geometry.size.height)

                CircleText(text: R.string.localizable.pause_title(),
                           radius: 150,
                           rotationDegree: -130,
                           flip: true)
                .position(x: geometry.size.width / 2,
                          y: geometry.size.height / 2)
                .frame(width: geometry.size.width,
                       height: geometry.size.height)

                CircleText(text: R.string.localizable.rest_title(),
                           radius: 150,
                           rotationDegree: -12,
                           flip: false)
                .position(x: geometry.size.width / 2,
                          y: geometry.size.height / 2)
                .frame(width: geometry.size.width,
                       height: geometry.size.height)

            }
        }
    }
}

struct CircularButtonView_Previews: PreviewProvider {
    static var previews: some View {
        CircularButtonView(isPickerShown: .constant(false),
                           pickerEnum: .constant(.workout),
                           workoutTime: .constant(10.1),
                           pauseTime: .constant(12.1),
                           restTime: .constant(12.1))
    }
}
