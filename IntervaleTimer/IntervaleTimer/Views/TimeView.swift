//
//  TimeView.swift
//  IntervaleTimer
//
//  Created by Hadi KUDSI on 28/04/2023.
//

import SwiftUI

struct TimeView: View {

    @Binding var repetitionSet: RepetitionSet
    @Binding var repPickerModel: PickerModel
    @State private var isTapped = false

    var body: some View {
        HStack {
            Text(R.string.localizable.repetitions_title() + ": \(repetitionSet.remainingRep)")
                .padding(.leading)
                .foregroundColor(.white)
            Spacer()
            Text(R.string.localizable.sets_title() + ": \(repetitionSet.remainingSet)")
                .padding(.trailing)
                .foregroundColor(.white)
        }
        .frame(width: 300, height: 50)
        .background(
            ZStack {
                LinearGradient(gradient: Gradient(colors: [.white.opacity(0.3),
                                                           .clear]),
                               startPoint: .topLeading,
                               endPoint: .bottomTrailing)
                .cornerRadius(10)
                .overlay(
                    RoundedRectangle(cornerRadius: 10)
                        .stroke(Color.black, lineWidth: 2)
                        .background(RoundedRectangle(cornerRadius: 10).fill(.gray))
                )
            }
        )
        .onTapGesture {
            repPickerModel.isPickerShown = true
        }
        .scaleEffect(isTapped ? 1.2 : 1.0)
        .onChange(of: repPickerModel.isPickerShown) { newValue in
                withAnimation {
                    isTapped = newValue
                }

        }
    }
}

struct TimeView_Previews: PreviewProvider {
    static var previews: some View {
        TimeView(repetitionSet: .constant(RepetitionSet()),
                 repPickerModel: .constant(PickerModel()))
    }
}
