//
//  RepetitionsPickerView.swift
//  IntervaleTimer
//
//  Created by Hadi KUDSI on 02/05/2023.
//

import SwiftUI

struct RepetitionsPickerView: View {

    @Binding var repetitionSet: RepetitionSet
    @Binding var repetitionPicker: PickerModel

    var body: some View {
        NavigationView {
            HStack {
                Spacer()

                Picker("", selection: $repetitionSet.repetitions) {
                    ForEach(1..<31, id: \.self) { input in
                        Text("\(input) " + R.string.localizable.repetition_title()).tag(input)
                    }
                }
                .pickerStyle(WheelPickerStyle())
                .frame(width: 180)
                .clipped()

                Picker("", selection: $repetitionSet.sets) {
                    ForEach(1..<31, id: \.self) { input in
                        Text("\(input) " + R.string.localizable.set_title()).tag(input)
                    }
                }
                .pickerStyle(WheelPickerStyle())
                .frame(width: 180)
                .clipped()

                Spacer()
            }
            .navigationTitle(Text(""))
            .navigationBarItems(trailing:
                                    Button(action: {
                repetitionPicker.isPickerShown = false
                repetitionPicker.doneTapped = true
            }) {
                Text(R.string.localizable.done_title())
                    .font(.headline)
                    .foregroundColor(.green)
            })
        }
    }
}

struct RepetitionsPickerView_Previews: PreviewProvider {
    static var previews: some View {
        RepetitionsPickerView(repetitionSet: .constant(RepetitionSet()),
                              repetitionPicker: .constant(PickerModel()))
    }
}
