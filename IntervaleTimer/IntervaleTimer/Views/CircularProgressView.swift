//
//  CircularProgressView.swift
//  IntervaleTimer
//
//  Created by Hadi KUDSI on 18/04/2023.
//

import SwiftUI

struct CircularProgressView: View {

    @Binding var progress: CircularProgressModel
    @Binding var workoutTime: CGFloat
    var workoutTimeString: String {
        String(format: "%02d:%02d", Int(workoutTime / 60),
               Int(workoutTime.truncatingRemainder(dividingBy: 60) + 1))
    }
    @Binding var pauseTime: CGFloat
    var pauseTimeString: String {
        String(format: "%02d:%02d", Int(pauseTime / 60),
               Int(pauseTime.truncatingRemainder(dividingBy: 60) + 1))
    }
    @Binding var restTime: CGFloat
    var restTimeString: String {
        String(format: "%02d:%02d", Int(restTime / 60),
               Int(restTime.truncatingRemainder(dividingBy: 60) + 1))
    }

    var body: some View {

        ZStack {
            GeometryReader { geometry in
                let size = min(geometry.size.width, geometry.size.height)
                let lineWidth: CGFloat = 50

                Circle()
                    .stroke(Color.red,
                            lineWidth: lineWidth + 5)
                    .frame(width: size - lineWidth, height: size - lineWidth)
                    .position(x: geometry.size.width / 2,
                              y: geometry.size.height / 2)

                Circle()
                    .stroke(Color.black,
                            lineWidth: lineWidth)
                    .frame(width: size - lineWidth,
                           height: size - lineWidth)
                    .opacity(0.8)
                    .position(x: geometry.size.width / 2,
                              y: geometry.size.height / 2)

                Circle()
                    .trim(from: progress.green, to: 0.5)
                    .stroke(Color.green,
                            lineWidth: lineWidth)
                    .frame(width: size - lineWidth,
                           height: size - lineWidth)
                    .rotationEffect(.degrees(-90))
                    .position(x: geometry.size.width / 2,
                              y: geometry.size.height / 2)

                Circle()
                    .trim(from: progress.red, to: 0.5)
                    .stroke(Color.red,
                            lineWidth: lineWidth)
                    .frame(width: size - lineWidth,
                           height: size - lineWidth)
                    .rotationEffect(.degrees(90))
                    .position(x: geometry.size.width / 2,
                              y: geometry.size.height / 2)

                Circle()
                    .fill(Color.white)
                    .frame(width: size - lineWidth,
                           height: size - lineWidth)
                    .position(x: geometry.size.width / 2,
                              y: geometry.size.height / 2)

                if progress.yellow > 0 {
                    Circle()
                        .fill(.yellow)
                        .frame(width: (size - lineWidth) -
                               ((size - lineWidth) * progress.yellow),
                               height: (size - lineWidth) -
                               ((size - lineWidth) * progress.yellow))
                        .position(x: geometry.size.width / 2,
                                  y: geometry.size.height / 2)
                }

                VStack {
                    if restTime > 0 {
                        Text(restTimeString)
                        Text(R.string.localizable.rest_title())
                            .bold()
                    } else if workoutTime > 0 {
                        Text(workoutTimeString)
                        Text(R.string.localizable.workout_title())
                            .bold()
                    } else if pauseTime > 0 {
                        Text(pauseTimeString)
                        Text(R.string.localizable.pause_title())
                            .bold()
                    }
                }
                .position(x: geometry.size.width / 2,
                          y: geometry.size.height / 2)
            }
        }
    }
}

struct CircularProgressView_Previews: PreviewProvider {
    static var previews: some View {
        CircularProgressView(progress: .constant(CircularProgressModel()),
                             workoutTime: .constant(0),
                             pauseTime: .constant(0),
                             restTime: .constant(0))
    }
}
