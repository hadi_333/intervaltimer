//
//  SetsHolder.swift
//  IntervaleTimer
//
//  Created by Hadi KUDSI on 06/06/2023.
//

import SwiftUI

struct SetsHolder: View {
    @ObservedObject var viewModel: IntervaleTimerCellViewModel

    var remainingTimeString: String {
        let oneSet = viewModel.workoutTimer.initialTime + viewModel.pauseTimer.initialTime
        let oneRep = (oneSet * CGFloat(viewModel.repetitionSet.initialSet)) + viewModel.restTimer.initialTime

        let remaining = oneRep * CGFloat(viewModel.repetitionSet.remainingRep) -
        (oneSet * CGFloat((viewModel.repetitionSet.initialSet + 1) - viewModel.repetitionSet.remainingSet)) +
        (viewModel.workoutTimer.timeRemaining + viewModel.pauseTimer.timeRemaining) -
        viewModel.restTimer.initialTime + viewModel.restTimer.timeRemaining

         return String(format: "%02d:%02d", Int(remaining / 60),
               Int(remaining.truncatingRemainder(dividingBy: 60)))
    }

    var totalTimeString: String {
        let totalTime = (((viewModel.workoutTimer.initialTime + viewModel.pauseTimer.initialTime) *
                              CGFloat(viewModel.repetitionSet.initialSet) +
                          viewModel.restTimer.initialTime) * CGFloat(viewModel.repetitionSet.initialRep))
         return String(format: "%02d:%02d", Int(totalTime / 60),
               Int(totalTime.truncatingRemainder(dividingBy: 60)))
    }

    var body: some View {
        ZStack {
            Rectangle()
                .foregroundColor(.black)
                .cornerRadius(10)
            VStack {
                if viewModel.repetitionSet.initialSet == 1 {
                    Text(String(viewModel.repetitionSet.initialSet) + " " + R.string.localizable.set_title())
                        .foregroundColor(.white)
                        .padding(.top)
                } else {
                    Text(String(viewModel.repetitionSet.initialSet) + " " + R.string.localizable.sets_title())
                        .foregroundColor(.white)
                        .padding(.top)
                }

                LinearGradient(colors: [.black, .white, .black],
                               startPoint: .leading,
                               endPoint: .trailing)
                .frame(height: 1)

                HStack {
                    VStack {
                        Text(remainingTimeString)
                            .foregroundColor(.white)
                        Text("Timer remaining")
                            .foregroundColor(.gray)
                    }
                    .padding([.leading, .bottom])
                    Spacer()
                    VStack {
                        Text(totalTimeString)
                            .foregroundColor(.white)
                        Text("Total time")
                            .foregroundColor(.gray)
                            .padding([.trailing, .bottom])
                    }
                }
            }
        }
    }
}

struct SetsHolder_Previews: PreviewProvider {
    static var previews: some View {
        SetsHolder(viewModel: (IntervaleTimerCellViewModel()))
    }
}
