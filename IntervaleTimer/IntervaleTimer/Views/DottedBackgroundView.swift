//
//  DottedBackgroundView.swift
//  IntervaleTimer
//
//  Created by Hadi KUDSI on 08/06/2023.
//

import SwiftUI

struct DottedBackgroundView: View {
    let dotSpacing: CGFloat = 7
    let dotSize: CGFloat = 3

    var body: some View {
        ZStack {
            Color.gray
            GeometryReader { geometry in
                Path { path in
                    let columns = Int(geometry.size.width / dotSpacing)
                    let rows = Int(geometry.size.height / dotSpacing)
                    let startX = (geometry.size.width - CGFloat(columns) * dotSpacing) / 2
                    let startY = (geometry.size.height - CGFloat(rows) * dotSpacing) / 2

                    for row in 0..<rows {
                        for column in 0..<columns {
                            let posX = startX + CGFloat(column) * dotSpacing
                            let posY = startY + CGFloat(row) * dotSpacing
                            let center = CGPoint(x: posX, y: posY)
                            var dotRect: CGRect
                            if row % 2 == 0 {
                                dotRect = CGRect(x: center.x - dotSize / 2,
                                                     y: center.y - dotSize / 2,
                                                     width: dotSize, height: dotSize)
                            } else {
                                dotRect = CGRect(x: center.x - dotSize / 2 + 5,
                                                     y: center.y - dotSize / 2,
                                                     width: dotSize, height: dotSize)
                            }
                            path.addEllipse(in: dotRect)

                        }
                    }
                }
                .fill(Color.black)
            }
        }
    }
}

struct DottedBackgroundView_Previews: PreviewProvider {
    static var previews: some View {
        DottedBackgroundView()
    }
}
