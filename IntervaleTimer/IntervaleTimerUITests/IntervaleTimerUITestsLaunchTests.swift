//
//  IntervaleTimerUITestsLaunchTests.swift
//  IntervaleTimerUITests
//
//  Created by Hadi KUDSI on 01/06/2023.
//

import XCTest

final class IntervaleTimerUITestsLaunchTests: XCTestCase {

    override class var runsForEachTargetApplicationUIConfiguration: Bool {
        true
    }

    override func setUpWithError() throws {
        continueAfterFailure = false
    }
}
