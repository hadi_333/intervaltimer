//
//  IntervaleTimerViewModelTest.swift
//  IntervaleTimerTests
//
//  Created by Hadi KUDSI on 26/05/2023.
//

import XCTest
@testable import IntervaleTimer

final class IntervaleTimerViewModelTest: XCTestCase {

    var sut: IntervaleTimerViewModel?

    @MainActor override func setUpWithError() throws {
        self.sut = IntervaleTimerViewModel()
    }

    override func tearDownWithError() throws {
        self.sut = nil
    }

    @MainActor func testAddNewTimer() {
        sut?.addNewTimer()
        XCTAssertEqual(sut?.intervaleTimers.count, 2)
        XCTAssertTrue(sut?.listTimersId.contains(sut?.intervaleTimers[1].id ?? UUID()) ?? false)
    }

    @MainActor func testDeleteTimer() {
        let timer = IntervaleTimerCellViewModel()
        sut?.intervaleTimers = [timer]
        sut?.listTimersId = [timer.id]

        sut?.deleteTimer()

        XCTAssertEqual(sut?.intervaleTimers.count, 0)
        XCTAssertEqual(sut?.listTimersId.count, 0)
    }

    @MainActor func testHandleWorkoutStatusChange() {
        let timer1 = IntervaleTimerCellViewModel()
        let timer2 = IntervaleTimerCellViewModel()
        sut?.intervaleTimers = [timer1, timer2]

        sut?.handleWorkoutStatusChange()

        XCTAssertFalse(sut?.isTimerOn ?? true)

        timer1.workoutStatus = .start
        timer2.workoutStatus = .pause

        sut?.handleWorkoutStatusChange()

        XCTAssertTrue(sut?.isTimerOn ?? false)
    }
}
