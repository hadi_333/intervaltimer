//
//  IntervaleTimerCellViewModelTest.swift
//  IntervaleTimerTests
//
//  Created by Hadi KUDSI on 26/05/2023.
//

import XCTest
@testable import IntervaleTimer

final class IntervaleTimerCellViewModelTest: XCTestCase {

    var sut: IntervaleTimerCellViewModel?

    @MainActor override func setUpWithError() throws {
        self.sut = IntervaleTimerCellViewModel()
    }

    override func tearDownWithError() throws {
        self.sut = nil
    }

    @MainActor func testSetProgress() {
        sut?.workoutTimer.initialTime = 60
        sut?.workoutTimer.timeRemaining = 30
        sut?.pauseTimer.initialTime = 40
        sut?.pauseTimer.timeRemaining = 20
        sut?.restTimer.initialTime = 20
        sut?.restTimer.timeRemaining = 10

        sut?.setProgress()

        XCTAssertEqual(sut?.progressModel.green, 0.25)
        XCTAssertEqual(sut?.progressModel.red, 0.25)
        XCTAssertEqual(sut?.progressModel.yellow, 0.5)
    }

    @MainActor func testTimerRemainingRepAndSetStartWorkout() {
        sut?.repetitionSet.remainingRep = 2
        sut?.repetitionSet.remainingSet = 2
        sut?.workoutStatus = .start
        sut?.restTimer.timeRemaining = 0
        sut?.workoutTimer.timeRemaining = 10
        sut?.pauseTimer.timeRemaining = 0

        sut?.timer()

        XCTAssertEqual(sut?.workoutTimer.timeRemaining, 9.99)
        XCTAssertEqual(sut?.pauseTimer.timeRemaining, 0)
        XCTAssertEqual(sut?.restTimer.timeRemaining, 0)
        XCTAssertEqual(sut?.restTimer.initialTime, 1)
        XCTAssertEqual(sut?.repetitionSet.remainingSet, 2)
    }

    @MainActor func testTimerRemainingRepAndSetStartRest() {
        sut?.repetitionSet.remainingRep = 2
        sut?.repetitionSet.remainingSet = 2
        sut?.workoutStatus = .start
        sut?.restTimer.timeRemaining = 10
        sut?.workoutTimer.timeRemaining = 10
        sut?.pauseTimer.timeRemaining = 0

        sut?.timer()

        XCTAssertEqual(sut?.workoutTimer.timeRemaining, 10)
        XCTAssertEqual(sut?.pauseTimer.timeRemaining, 0)
        XCTAssertEqual(sut?.restTimer.timeRemaining, 9.99)
        XCTAssertEqual(sut?.restTimer.initialTime, 1)
        XCTAssertEqual(sut?.repetitionSet.remainingSet, 2)
    }

    @MainActor func testTimerRemainingRepAndSetStartPause() {
        sut?.repetitionSet.remainingRep = 2
        sut?.repetitionSet.remainingSet = 2
        sut?.workoutStatus = .start
        sut?.restTimer.timeRemaining = 0
        sut?.workoutTimer.timeRemaining = 0
        sut?.pauseTimer.timeRemaining = 10

        sut?.timer()

        XCTAssertEqual(sut?.workoutTimer.timeRemaining, 0)
        XCTAssertEqual(sut?.pauseTimer.timeRemaining, 9.99)
        XCTAssertEqual(sut?.restTimer.timeRemaining, 0)
        XCTAssertEqual(sut?.restTimer.initialTime, 1)
        XCTAssertEqual(sut?.repetitionSet.remainingSet, 2)
    }

    @MainActor func testTimerRemainingRepAndSetPause() {
        sut?.workoutStatus = .pause

        sut?.timer()

    }

    @MainActor func testTimerRemainingRepAndSetStop() {
        sut?.repetitionSet.remainingRep = 2
        sut?.repetitionSet.remainingSet = 2
        sut?.workoutStatus = .stop
        sut?.restTimer.timeRemaining = 0
        sut?.workoutTimer.timeRemaining = 0
        sut?.pauseTimer.timeRemaining = 10

        sut?.timer()

        XCTAssertEqual(sut?.workoutTimer.timeRemaining, 1)
        XCTAssertEqual(sut?.pauseTimer.timeRemaining, 1)
        XCTAssertEqual(sut?.restTimer.timeRemaining, 1)
        XCTAssertEqual(sut?.restTimer.initialTime, 1)
        XCTAssertEqual(sut?.repetitionSet.remainingSet, 1)
    }

    @MainActor func testTimerRemainingRepStartRest() {
        sut?.repetitionSet.remainingRep = 2
        sut?.repetitionSet.remainingSet = 0
        sut?.workoutStatus = .start
        sut?.restTimer.timeRemaining = 10

        sut?.timer()

        XCTAssertEqual(sut?.restTimer.timeRemaining, 9.99)
    }

    @MainActor func testTimerRemainingRepStartNoRest() {
        sut?.repetitionSet.remainingRep = 2
        sut?.repetitionSet.remainingSet = 0
        sut?.repetitionSet.initialSet = 2
        sut?.workoutStatus = .start
        sut?.restTimer.timeRemaining = 0
        sut?.restTimer.initialTime = 10

        sut?.timer()

        XCTAssertEqual(sut?.restTimer.timeRemaining, sut?.restTimer.initialTime)
        XCTAssertEqual(sut?.repetitionSet.remainingSet, sut?.repetitionSet.initialSet)
        XCTAssertEqual(sut?.repetitionSet.remainingRep, 1)
    }
    @MainActor func testTimerNoRep() {
        sut?.repetitionSet.remainingRep = 0
        sut?.repetitionSet.initialRep = 2
        sut?.repetitionSet.remainingSet = 0
        sut?.repetitionSet.initialSet = 2
        sut?.workoutStatus = .start

        sut?.timer()

        XCTAssertEqual(sut?.workoutStatus, .ended)
        XCTAssertEqual(sut?.repetitionSet.remainingSet, sut?.repetitionSet.initialSet)
        XCTAssertEqual(sut?.repetitionSet.remainingRep, sut?.repetitionSet.initialRep)
    }

    @MainActor func testDidDismissTimePickerDoneTappedWorkout() {
        sut?.timePickerModel.doneTapped = true
        sut?.pickerEnum = .workout
        sut?.workoutTimer.minutes = 1
        sut?.workoutTimer.seconds = 30
        sut?.workoutTimer.oldSec = 30
        sut?.workoutTimer.oldMin = 1

        sut?.didDismissTimePicker()

        XCTAssertEqual(sut?.workoutTimer.initialTime, 90)
        XCTAssertEqual(sut?.workoutTimer.timeRemaining, 90)
        XCTAssertEqual(sut?.workoutTimer.oldSec, 30)
        XCTAssertEqual(sut?.workoutTimer.minutes, 1)
        XCTAssertTrue(((sut?.timePickerModel.doneTapped) != nil))
    }

    @MainActor func testDidDismissTimePickerDoneTappedPause() {
        sut?.timePickerModel.doneTapped = true
        sut?.pickerEnum = .pause
        sut?.pauseTimer.minutes = 1
        sut?.pauseTimer.seconds = 30
        sut?.pauseTimer.oldSec = 30
        sut?.pauseTimer.oldMin = 1

        sut?.didDismissTimePicker()

        XCTAssertEqual(sut?.pauseTimer.initialTime, 90)
        XCTAssertEqual(sut?.pauseTimer.timeRemaining, 90)
        XCTAssertEqual(sut?.pauseTimer.oldSec, 30)
        XCTAssertEqual(sut?.pauseTimer.minutes, 1)
        XCTAssertTrue(((sut?.timePickerModel.doneTapped) != nil))
    }

    @MainActor func testDidDismissTimePickerDoneTappedRest() {
        sut?.timePickerModel.doneTapped = true
        sut?.pickerEnum = .rest
        sut?.restTimer.minutes = 1
        sut?.restTimer.seconds = 30
        sut?.restTimer.oldSec = 30
        sut?.restTimer.oldMin = 1

        sut?.didDismissTimePicker()

        XCTAssertEqual(sut?.restTimer.initialTime, 90)
        XCTAssertEqual(sut?.restTimer.timeRemaining, 90)
        XCTAssertEqual(sut?.restTimer.oldSec, 30)
        XCTAssertEqual(sut?.restTimer.minutes, 1)
        XCTAssertTrue(((sut?.timePickerModel.doneTapped) != nil))
    }

    @MainActor func testDidDismissTimePickerDoneNotTappedWorkout() {
        sut?.timePickerModel.doneTapped = false
        sut?.pickerEnum = .workout
        sut?.workoutTimer.minutes = 2
        sut?.workoutTimer.seconds = 50
        sut?.workoutTimer.oldSec = 30
        sut?.workoutTimer.oldMin = 1

        sut?.didDismissTimePicker()

        XCTAssertEqual(sut?.workoutTimer.seconds, sut?.workoutTimer.oldSec)
        XCTAssertEqual(sut?.workoutTimer.minutes, sut?.workoutTimer.oldMin)
    }

    @MainActor func testDidDismissTimePickerDoneNotTappedPause() {
        sut?.timePickerModel.doneTapped = false
        sut?.pickerEnum = .pause
        sut?.pauseTimer.minutes = 2
        sut?.pauseTimer.seconds = 50
        sut?.pauseTimer.oldSec = 30
        sut?.pauseTimer.oldMin = 1

        sut?.didDismissTimePicker()

        XCTAssertEqual(sut?.pauseTimer.seconds, sut?.pauseTimer.oldSec)
        XCTAssertEqual(sut?.pauseTimer.minutes, sut?.pauseTimer.oldMin)
    }

    @MainActor func testDidDismissTimePickerDoneNotTappedRest() {
        sut?.timePickerModel.doneTapped = false
        sut?.pickerEnum = .rest
        sut?.restTimer.minutes = 2
        sut?.restTimer.seconds = 50
        sut?.restTimer.oldSec = 30
        sut?.restTimer.oldMin = 1

        sut?.didDismissTimePicker()

        XCTAssertEqual(sut?.restTimer.seconds, sut?.restTimer.oldSec)
        XCTAssertEqual(sut?.restTimer.minutes, sut?.restTimer.oldMin)
    }

    @MainActor func testDidDismissRepetitionPickerDoneTapped() {
        sut?.repetitionPickerModel.doneTapped = true
        sut?.repetitionSet.repetitions = 5
        sut?.repetitionSet.sets = 3
        sut?.repetitionSet.oldRepetition = 5
        sut?.repetitionSet.oldSet = 3

        sut?.didDismissRepetitionPicker()

        XCTAssertEqual(sut?.repetitionSet.initialRep, 5)
        XCTAssertEqual(sut?.repetitionSet.remainingRep, 5)
        XCTAssertEqual(sut?.repetitionSet.initialSet, 3)
        XCTAssertEqual(sut?.repetitionSet.remainingSet, 3)
    }

    @MainActor func testDidDismissRepetitionPickerDoneNotTapped() {
        sut?.repetitionPickerModel.doneTapped = false
        sut?.repetitionSet.repetitions = 5
        sut?.repetitionSet.sets = 3
        sut?.repetitionSet.oldRepetition = 8
        sut?.repetitionSet.oldSet = 8

        sut?.didDismissRepetitionPicker()

        XCTAssertEqual(sut?.repetitionSet.repetitions, 8)
        XCTAssertEqual(sut?.repetitionSet.sets, 8)
    }
}
