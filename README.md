# IntervalTimer



## Getting started

First, you should get this two things to get started:

 - XCode : _16.2_ 
 - Swift : _5.7.2_
 
## Install

Before Start, Yopu must have [xcodegen](https://github.com/yonaskolb/XcodeGen) tools to build project.
Launch following command lines to generate .xcodeproj from file setting _project.yml_

```bash
xcodegen && pod install
```

## Dependancies

### [R.swift](https://github.com/mac-cain13/R.swift)

Get strong typed, autocompleted resources like images, fonts and segues in Swift projects

### [XcodeGen](https://github.com/yonaskolb/XcodeGen)

A Swift command line tool for generating your Xcode project
